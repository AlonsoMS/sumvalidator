const array = [3, 5, -4, 8, 11, 1, -1, 6]
const targetSum = 10
function twoNumberSum(array, targetSum) { 
    for(let p=0; p < array.length-1; p++) {
        for(let s=p+1;s<array.length;s++){
            if (array[p]+array[s]==targetSum){
                return [array[p],array[s]]
            }
        }
    }
}

const result = twoNumberSum(array, targetSum)
console.log("El array resultante es: "+ result)
